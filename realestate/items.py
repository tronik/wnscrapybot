# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class RealestateItem(Item):
    # define the fields for your item 

    first_name = Field()
    last_name = Field()
    email = Field()
    phone = Field()
    street1 = Field()
    city = Field()
    county = Field()
    state = Field()
    zipcode = Field()
    property_type = Field()
    livein = Field()
    short_term = Field()
    year_built = Field()
    square_footage = Field()
    roof_year = Field()
    hvac_year = Field()
    electrical_year = Field()
    plumbing_year = Field()
    circuit = Field()
    commercial = Field()
    damage = Field()
    renovation = Field()
    claims = Field()
    structure = Field()
    income = Field()
    finishes = Field()
    status = Field()

    pass
