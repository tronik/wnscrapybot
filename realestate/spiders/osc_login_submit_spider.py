import scrapy
from scrapy.http import FormRequest
from scrapy.utils.response import open_in_browser
from realestate.items import RealestateItem

class LoginSpider(scrapy.Spider):
    name = "oscloginandsubmit"
    allowed_domain = "homechoicecomplete.com"
    start_urls = ["https://app.homechoicecomplete.com/Insurance/RealProtect.nsf?OpenDatabase&login", ]
    item = RealestateItem()

    def parse(self, response):
            yield FormRequest(
            "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf?OpenDatabase&login", 
            formdata={'%%ModDate': '0000000000000000', 'Username': 'username1', 'Password': 'password1'}, 
        callback=self.after_login
        )

    def after_login(self, response):
        #create_new_button = response.xpath('//*[@id="view:_id1:_id2:_id40:_id41:HiddenBtnApprove"]').get()
        next_url = "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/InitialReview.xsp"
        yield FormRequest(next_url, callback=self.submit_form)
    
    def submit_form(self, response):
        new_doc_url = "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/RequestForm_Admin.xsp?action=newDocument"
        viewid = response.xpath('//*[@id="view:_id1__VUID"]/@value').get()
        xspsubmitid = response.xpath('/html/body/form/input[2]/@value').get()
        hidden22 = response.xpath('/html/body/form/input[2]').get()
        hidden5 = response.xpath('//*[@id="view:_id1"]/input[5]').get()
        session_id_cookie = response.headers.getlist('Set-Cookie')[0].decode('utf8').split(';')[0]
        cookie = response.headers.getlist('Set-Cookie')[0].decode("utf-8").split(";")[0].split("=")

        insured_name = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredName"]/@value').get()
        insured_email = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredEmailID"]/@value').get()
        insured_phone = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredPhone"]/@value').get()
        myformdata = {
            '$$viewid': viewid,
            'view:_id1:_id2:_id40:InsuredName': 'James Smith',
            'view:_id1:_id2:_id40:InsuredEmailID': 'James@realpro.com',
            'view:_id1:_id2:_id40:InsuredPhone': '(111)123-1234',
            'view:_id1:_id2:_id40:propAddress1': '3635 Mitchell Lake Drive', #Street1
            'view:_id1:_id2:_id40:propAddress2': '', #Street2
            'view:_id1:_id2:_id40:propCity1': 'Gainesville', #City
            'view:_id1:_id2:_id40:propState1': 'GA', #State. This will be used to calculate the price per sqrt ($115 for all, $125 for TX, and $150 for CA)
            'view:_id1:_id2:_id40:propZip1': '30506', #Zipcode
            'view:_id1:_id2:_id40:propCounty1': 'Hall', #County
            'view:_id1:_id2:_id40:propType1': 'Rental Property', #Property Type
            'view:_id1:_id2:_id40:BuildingType1': 'SFR', #calculated field SFR, MU, CND
            'view:_id1:_id2:_id40:propConType1': 'Wood Frame', #structure
            'view:_id1:_id2:_id40:propSqrFootage': '1900', #use this number to calc total value sqft * 115 (or 125 for TX or 150 for CA)
            'view:_id1:_id2:_id40:PropMonthlyRent': '2000', #Income (monthly)
            'view:_id1:_id2:_id40:propNumUnits': '1', # we did not collect this??? We need to decide on a default
            'view:_id1:_id2:_id40:propYearBuilt1': '1987', #Year built
            'view:_id1:_id2:_id40:propPlumbRenYear': '2015', #Plumbing year
            'view:_id1:_id2:_id40:ageOfRoof': '15', #This may need to be calculated bused on calander year if they need age of the roof in number of years
            'view:_id1:_id2:_id40:ElectricalCircuitBreakers': '2016', #Electrical yea
            'view:_id1:_id2:_id40:rbDamageToHome': 'No', #Damage
            'view:_id1:_id2:_id40:propertyLossRb': 'No', #Any major claims
            'view:_id1:_id2:_id40:IsCovNewPurChase': 'Yes', #Provide binder for closing. We did not collect this. We will use the default
            'view:_id1:_id2:_id40:comboBox1': '1', # we did not collect this (0=new, 1=auto renewal, and 2=new renewal I think we will use a default. Need to ask)
            'view:_id1:_id2:_id40:policyTerm1': '6 Months', #We did not collect this! So is 6 month is the default or 1 year?
            'view:_id1:_id2:_id40:CoverageAmount1': '218500', # this is the cacluated field (price of sqft * sqft) to get the total coverage. See the price per sqft above
            'view:_id1:_id2:_id40:lossOfRentalIncome1': '24000', #This is a calculated field (monthlyRent * 12) to get the annual income from property
            'view:_id1:_id2:_id40:ContCovAmtReq1': '8,888', # Content Coverage Amount. we did not collect this we need a default
            'view:_id1:_id2:_id40:Liability': '1000000', #We did not collect Liability. We will use the default $1000,000
            'view:_id1:_id2:_id40:dwellCovAmtReq1': '242500', #This is a calculate field (coverage amount from $sqft * #sqft + annula rental income from income * 12) 
            'view:_id1:_id2:_id40:zillowPropAmount1': '',  #did not collect this. Use the default value
            'view:_id1:_id2:_id40:SewerBackupRG': 'No',  #did not collect this. Use the default value
            'view:_id1:_id2:_id40:OrdinanceLawRG': 'No',  #did not collect this. Use the default value
            }
        # May need to use this button: clickdata={"name":"view:_id1:_id2:_id40:button14"}
        yield FormRequest(new_doc_url, formdata = myformdata, callback=self.show_elements, headers={'Cookie': session_id_cookie, 'Referer': 'https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/InitialReview.xsp'})
    
    def show_elements(self, response):
         print('______________________ All Done! _________________________')
         quote1 = response.xpath('/html/body/form/div[2]/div/div/div/div/div/div[3]/div[3]/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td[2]/span').get()
         quote2 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField22"]/@value').get() # this is the quote for $2,500 deductable
         quote3 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField26"]/@value').get() # this is the quote for $5,000 deductable
         quote4 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField30"]/@value').get() # this is the quote for $7,500 deductable
         print(f'Quote1: {quote1}')
         print(f'Quote2: {quote2}')
         print(f'Quote3: {quote3}')
         print(f'Quote4: {quote4}')
         print('______________________ All Done! _________________________')
