import scrapy
from scrapy.http import FormRequest
from scrapy.utils.response import open_in_browser

class LoginSpider(scrapy.Spider):
    name = "autologin"
    allowed_domain = "http://localhost:8000"
    start_urls = ["http://localhost:8000/admin/login/?next=/admin/", ]

    def parse(self, response):
        csrf_token = response.css("input[name='csrfmiddlewaretoken']").attrib["value"]
        yield FormRequest(
            "http://localhost:8000/admin/login/?next=/admin/",
            formdata={'csrfmiddlewaretoken': csrf_token, 'username': self.settings.get('USERNAME'), 'password': self.settings.get('PASSWORD')},
            #formdata={'csrfmiddlewaretoken': csrf_token, 'username': 'user', 'password': 'pass!'},
            callback=self.after_login
        )
    
    def after_login(self, response):
        page_url = response.css("tr.model-contactrequest td a.addlink").attrib["href"]
        next_page = f"{self.allowed_domain}{page_url}"
        csrf_token = response.css("input[name='csrfmiddlewaretoken']").attrib["value"]
        yield FormRequest(next_page, formdata = {'csrfmiddlewaretoken': csrf_token, 'contact_full_name': 'James Francis', 'contact_email': 'jamesFrances@yahoo.com',}, callback=self.submit_form)
    
    def submit_form(self, response):
        print("_________________________form should be submitted______________________________")
        header = response.xpath('/html/body').get()
        #print(header)
        open_in_browser(response)

      