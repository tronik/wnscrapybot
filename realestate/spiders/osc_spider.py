import scrapy
from scrapy.http import FormRequest
from scrapy.utils.response import open_in_browser
from realestate.items import RealestateItem


class LoginSpider(scrapy.Spider):
    name = "oscspider"
    allowed_domains = ["homechoicecomplete.com", "localhost"]
    start_urls = ["http://localhost:8000/admin/login/?next=/admin/", ]
    item = RealestateItem()

    def parse(self, response):
            csrf_token = response.css("input[name='csrfmiddlewaretoken']").attrib["value"]
            yield FormRequest(
                "http://localhost:8000/admin/login/?next=/admin/",
                formdata={'csrfmiddlewaretoken': csrf_token, 'username': self.settings.get('USERNAME'), 'password': self.settings.get('PASSWORD')},
                #formdata={'csrfmiddlewaretoken': csrf_token, 'username': 'user', 'password': 'pass'},
                callback=self.after_login
            )
    
    def after_login(self, response):
        next_page = 'http://localhost:8000/admin/realprotect/completequoterequest/53/change/'
        csrf_token = response.css("input[name='csrfmiddlewaretoken']").attrib["value"]
        yield FormRequest(next_page, callback=self.submit_form)
    
    def submit_form(self, response):
        #item = RealestateItem()

        print("_________________________form should be submitted______________________________")
        first_name = response.css('#id_comp_req_first_name').attrib['value']
        last_name = response.css('#id_comp_req_last_name').attrib['value']
        email = response.css('#id_comp_req_email').attrib['value']
        phone = response.css('#id_comp_req_phone').attrib['value']
        street1 = response.css('#id_comp_req_street1').attrib['value']
        city = response.css('#id_comp_req_city').attrib['value']
        state = response.css('#id_comp_req_state').attrib['value']
        zipcode = response.css('#id_comp_req_zipcode').attrib['value']
        county = response.css('#id_comp_req_county').attrib['value']
        property_type = response.css('#id_comp_req_property_type').attrib['value']
        livein = response.css('#id_comp_req_livein').attrib['value']
        short_term = response.css('#id_comp_req_short_term').attrib['value']
        year_built = response.css('#id_comp_req_year_built').attrib['value']
        square_footage = response.css('#id_comp_req_square_footage').attrib['value']
        roof_year = response.css('#id_comp_req_roof_year').attrib['value']
        hvac_year = response.css('#id_comp_req_hvac_year').attrib['value']
        electrical_year = response.css('#id_comp_req_electrical_year').attrib['value']
        plumbing_year = response.css('#id_comp_req_plumbing_year').attrib['value']
        circuit = response.css('#id_comp_req_circuit').attrib['value']
        commercial = response.css('#id_comp_req_commercial').attrib['value']
        damage = response.css('#id_comp_req_damage').attrib['value']
        renovation = response.css('#id_comp_req_renovation').attrib['value']
        claims = response.css('#id_comp_req_claims').attrib['value']
        structure = response.css('#id_comp_req_structure').attrib['value']
        income = response.css('#id_comp_req_income').attrib['value']
        finishes = response.css('#id_comp_req_finishes').attrib['value']
        status = response.css('#id_comp_req_status').attrib['value']

        print(f'Name: {first_name} {last_name}')
        print(f'Email: {email} Phone: {phone}')
        print(f'Address: {street1} {city} {county} {state} {zipcode}')
        print(f'Property type: {property_type}')
        print(f'Live in: {livein}')
        print(f'Short term: {short_term}')
        print(f'Year built: {year_built}')
        print(f'Square footage: {square_footage}')
        print(f'Roof year: {roof_year}')
        print(f'Hvac year: {hvac_year}')
        print(f'Electrical year: {electrical_year}')
        print(f'Plumbing year: {plumbing_year}')
        print(f'Circuit: {circuit}')
        print(f'Commercial: {commercial}')
        print(f'Damage: {damage}')
        print(f'Renovation: {renovation}')
        print(f'Claims: {claims}')
        print(f'Structure: {structure}')
        print(f'Income: {income}')
        print(f'Finishes: {finishes}')
        print(f'Status: {status}')

        self.item['first_name'] = first_name
        self.item['last_name'] = last_name
        self.item['email'] = email
        self.item['phone'] = phone
        self.item['street1'] = street1
        self.item['city'] = city
        self.item['county'] = county
        self.item['state'] = state
        self.item['zipcode'] = zipcode
        self.item['property_type'] = property_type
        self.item['livein'] = livein        
        self.item['short_term'] = short_term
        self.item['year_built'] = year_built        
        self.item['square_footage'] = square_footage        
        self.item['roof_year'] = roof_year
        self.item['hvac_year'] = hvac_year
        self.item['electrical_year'] = electrical_year
        self.item['plumbing_year'] = plumbing_year
        self.item['circuit'] = circuit       
        self.item['commercial'] = commercial
        self.item['damage'] = damage
        self.item['renovation'] = renovation
        self.item['claims'] = claims
        self.item['structure'] = structure
        self.item['income'] = income
        self.item['finishes'] = finishes
        self.item['status'] = status

        yield self.item
        yield FormRequest(
            "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf?OpenDatabase&login", 
            formdata={'%%ModDate': '0000000000000000', 'Username': 'login', 'Password': 'pass'}, 
        callback=self.another_after_login
        )

    def another_after_login(self, response):

        #create_new_button = response.xpath('//*[@id="view:_id1:_id2:_id40:_id41:HiddenBtnApprove"]').get()
        next_url = "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/InitialReview.xsp"
        yield FormRequest(next_url, callback=self.another_submit_form)
    
    def another_submit_form(self, response):
        new_doc_url = "https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/RequestForm_Admin.xsp?action=newDocument"
        viewid = response.xpath('//*[@id="view:_id1__VUID"]/@value').get()
        xspsubmitid = response.xpath('/html/body/form/input[2]/@value').get()
        hidden22 = response.xpath('/html/body/form/input[2]').get()
        hidden5 = response.xpath('//*[@id="view:_id1"]/input[5]').get()
        session_id_cookie = response.headers.getlist('Set-Cookie')[0].decode('utf8').split(';')[0]
        cookie = response.headers.getlist('Set-Cookie')[0].decode("utf-8").split(";")[0].split("=")

        insured_name = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredName"]/@value').get()
        insured_email = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredEmailID"]/@value').get()
        insured_phone = response.xpath('//*[@id="view:_id1:_id2:_id40:InsuredPhone"]/@value').get()
        myformdata = {
            '$$viewid': viewid,
            'view:_id1:_id2:_id40:InsuredName': 'James Smith',
            'view:_id1:_id2:_id40:InsuredEmailID': 'James@realpro.com',
            'view:_id1:_id2:_id40:InsuredPhone': '(111)123-1234',
            'view:_id1:_id2:_id40:propAddress1': '3635 Mitchell Lake Drive', #Street1
            'view:_id1:_id2:_id40:propAddress2': '', #Street2
            'view:_id1:_id2:_id40:propCity1': 'Gainesville', #City
            'view:_id1:_id2:_id40:propState1': 'GA', #State. This will be used to calculate the price per sqrt ($115 for all, $125 for TX, and $150 for CA)
            'view:_id1:_id2:_id40:propZip1': '30506', #Zipcode
            'view:_id1:_id2:_id40:propCounty1': 'Hall', #County
            'view:_id1:_id2:_id40:propType1': 'Rental Property', #Property Type
            'view:_id1:_id2:_id40:BuildingType1': 'SFR', #calculated field SFR, MU, CND
            'view:_id1:_id2:_id40:propConType1': 'Wood Frame', #structure
            'view:_id1:_id2:_id40:propSqrFootage': '1900', #use this number to calc total value sqft * 115 (or 125 for TX or 150 for CA)
            'view:_id1:_id2:_id40:PropMonthlyRent': '2000', #Income (monthly)
            'view:_id1:_id2:_id40:propNumUnits': '1', # we did not collect this??? We need to decide on a default
            'view:_id1:_id2:_id40:propYearBuilt1': '1987', #Year built
            'view:_id1:_id2:_id40:propPlumbRenYear': '2015', #Plumbing year
            'view:_id1:_id2:_id40:ageOfRoof': '15', #This may need to be calculated bused on calander year if they need age of the roof in number of years
            'view:_id1:_id2:_id40:ElectricalCircuitBreakers': '2016', #Electrical yea
            'view:_id1:_id2:_id40:rbDamageToHome': 'No', #Damage
            'view:_id1:_id2:_id40:propertyLossRb': 'No', #Any major claims
            'view:_id1:_id2:_id40:IsCovNewPurChase': 'Yes', #Provide binder for closing. We did not collect this. We will use the default
            'view:_id1:_id2:_id40:comboBox1': '1', # we did not collect this (0=new, 1=auto renewal, and 2=new renewal I think we will use a default. Need to ask)
            'view:_id1:_id2:_id40:policyTerm1': '6 Months', #We did not collect this! So is 6 month is the default or 1 year?
            'view:_id1:_id2:_id40:CoverageAmount1': '218500', # this is the cacluated field (price of sqft * sqft) to get the total coverage. See the price per sqft above
            'view:_id1:_id2:_id40:lossOfRentalIncome1': '24000', #This is a calculated field (monthlyRent * 12) to get the annual income from property
            'view:_id1:_id2:_id40:ContCovAmtReq1': '8,888', # Content Coverage Amount. we did not collect this we need a default
            'view:_id1:_id2:_id40:Liability': '1000000', #We did not collect Liability. We will use the default $1000,000
            'view:_id1:_id2:_id40:dwellCovAmtReq1': '242500', #This is a calculate field (coverage amount from $sqft * #sqft + annula rental income from income * 12) 
            'view:_id1:_id2:_id40:zillowPropAmount1': '',  #did not collect this. Use the default value
            'view:_id1:_id2:_id40:SewerBackupRG': 'No',  #did not collect this. Use the default value
            'view:_id1:_id2:_id40:OrdinanceLawRG': 'No',  #did not collect this. Use the default value
            }

        print("_________________________form should be submitted______________________________")
        print(self.item['first_name'])
        print('_________________________ End of Print _____________________')
        # May need to use this button: clickdata={"name":"view:_id1:_id2:_id40:button14"}
        yield FormRequest(new_doc_url, formdata = myformdata, callback=self.show_elements, headers={'Cookie': session_id_cookie, 'Referer': 'https://app.homechoicecomplete.com/Insurance/RealProtect.nsf/InitialReview.xsp'})
    
    def show_elements(self, response):
         print('______________________ All Done! _________________________')

         quote1 = response.xpath('/html/body/form/div[2]/div/div/div/div/div/div[3]/div[3]/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td[2]/span').get()
         quote2 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField22"]/@value').get() # this is the quote for $2,500 deductable
         quote3 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField26"]/@value').get() # this is the quote for $5,000 deductable
         quote4 = response.xpath('//*[@id="view:_id1:_id2:_id40:computedField30"]/@value').get() # this is the quote for $7,500 deductable
         print(f'Quote1: {quote1}')
         print(f'Quote2: {quote2}')
         print(f'Quote3: {quote3}')
         print(f'Quote4: {quote4}')
         print('______________________ All Done! _________________________')